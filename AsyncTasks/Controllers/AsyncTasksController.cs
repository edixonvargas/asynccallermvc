﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;

namespace AsyncTasks.Controllers
{
    public class AsyncTasksController : Controller
    {

        public ActionResult ShowAsyncCaller()
        {
            return View("AsyncTaskCaller");
        }

        // GET: AsyncTasks
        [HttpPost]
        public async Task<ActionResult> AsyncRequest()
        {
            double _pi = await pi(1000000000);
            return Content("valor pi es: ");

            //return View("AsyncTaskCaller");
        }
        
        public Task<double> pi(int i)
        {
            double CurrentPi = 1;
            var multiplier = 1;
            var nextNumber = 3;

            double result = 1;
            int Iteration = 0;
            while (Iteration < i)
            {
                multiplier = multiplier * -1;
                result = result + multiplier * 1 / (nextNumber);
                CurrentPi = result * 4;
                
                Iteration++;
                nextNumber += 2;
            }
            return Task.FromResult(CurrentPi);
        }
    }
}