﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AsyncTasks.Startup))]
namespace AsyncTasks
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
